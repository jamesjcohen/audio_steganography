audioFileIn = audioread('chord.wav');
img = imread('lena.gif');


soundsc(audioFileIn,44100);
scaleFactor = 0.5
[r z] = size(img);

imgSmall = imresize(img, scaleFactor);
imgSmall = round(imgSmall/32)

figure
imshow(imgSmall*32)


flattenedImg = imgSmall(:)';

scale = 1;

[x y] = size(audioFileIn);

multiplierArray = ones(1,x);

i = 1;
while i <= length(flattenedImg)
    multiplierArray(i) = flattenedImg(i);
    i = i + 1;
end

audioFileOut = audioFileIn.*multiplierArray';

soundsc(audioFileOut,44100);

decrypt = (audioFileOut./audioFileIn);

% audiowrite('output',y,Fs)

whos decrypt flattenedImg

B = reshape(decrypt(1:size(flattenedImg')),r*scaleFactor,z*scaleFactor)
figure
imshow(B/8);
